$(function() {
    var pull = $('.nav-toggle');

    $(pull).on('click touchstart', function(e) {
        e.preventDefault();
        $('body').toggleClass('openNav');
        pull.toggleClass('open');
    });


    $('.openNav .page').on('click touchstart', function(e) {
        e.preventDefault();
        $('body').toggleClass('openNav');
        $('.header__toggle').removeClass('open');
    });
});


$(".btn-modal").fancybox({
  //  padding : 0,
    toolbar  : false,
    touch : {
        vertical : false,
        momentum : false
    },
    modal: true,

    btnTpl : {
        smallBtn : '<button data-fancybox-close class="modal__close" title="{{CLOSE}}"></button>'
    }
});

$(".btn-modal-opacity").fancybox({
    //  padding : 0,
    toolbar  : false,
    touch : {
        vertical : false,
        momentum : false
    },
    modal: true,

    btnTpl : {
        smallBtn : '<button data-fancybox-close class="modal__close" title="{{CLOSE}}"></button>'
    }
});


$('.modal__close').on('click touchstart', function(e) {
    $.fancybox.close(true);
});


$('.btn-consulate').fancybox({
    modal: true,
    afterShow: function( instance, slide ) {

        $('.consulate__slider').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            dots: false,
            arrows: true,
            variableWidth: true,
            adaptiveHeight: false,
            prevArrow: '<span class="slide-nav prev"></span>',
            nextArrow: '<span class="slide-nav next"></span>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false
                    }
                }
            ]
        });
    },
    beforeClose: function( instance, slide ) {
        $('.consulate__slider').slick('unslick');
    }

});



var slider = $('.slider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    dots: true,
    arrows: false,
    fade: true,
    adaptiveHeight: true,
    verticalSwiping: true
});


slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){

    var mySlide = '.bgDesktop' + nextSlide;


    $('.bgDesktop').find('.bgDesktop__item').removeClass('active');
    $('.bgDesktop').find(mySlide).addClass('active');
});


$('.homepage').bind('mousewheel DOMMouseScroll', function(event){

     var win_w = $(window).width();

    if (win_w > 768 ) {

        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            $('.slider').slick('slickPrev');
        }
        else {
            $('.slider').slick('slickNext');
        }
    }

    else {

    }
});

// City select

$('.citySelect').on('click touchstart', function(e) {
    e.preventDefault();
    console.log('click');
    $('.btnCitySelect').trigger('click');
});


jQuery(document).ready(function(){

    jQuery('.scrollbar-inner').scrollbar();


    $("input[type=file]").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $(this).closest('.file-label').find('span').text(filename);
    });

    jQuery('.scrollbar-wh').scrollbar({

    });
});


$('.gallerySlider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    dots: false,
    arrows: false,
    swipe: false,
    draggable: false,
    asNavFor: '.galleryThumbs__slider'
});

$('.galleryThumbs__slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    dots: false,
    arrows: false,
    focusOnSelect: true,
    asNavFor: '.gallerySlider'
});

$('.galleryThumbs__nav.prev').on('click touchstart', function(e) {
    e.preventDefault();
    $('.galleryThumbs__slider').slick('slickPrev');
});

$('.galleryThumbs__nav.next').on('click touchstart', function(e) {
    e.preventDefault();
    $('.galleryThumbs__slider').slick('slickNext');
});


$('.btn-view').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $(this).closest('.article').toggleClass('open');
});



$('.filter-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $('.filter').toggleClass('open');
});


$('.product__else').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $(this).closest('.product__text').toggleClass('open');
});





